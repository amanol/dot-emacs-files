;; TODO
;; - [ ] split setup for work-home pc
;; (profiler-start 'cpu)
;; .emacs

;;; uncomment this line to disable loading of "default.el" at startup
;; (setq inhibit-default-init t)

;; enable visual feedback on selections
;(setq transient-mark-mode t)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)

;; avoid yes-no questions
(fset 'yes-or-no-p 'y-or-n-p)

;; debug helpers
;;(setq debug-on-error t)
;;(setq debug-on-init t)
;;(setq debug-on-message "failed to define function org")

;; systems and runtimes
(defconst *is-mac* (string-equal system-type "darwin"))
(defconst *is-linux* (string-equal system-type "gnu/linux"))
(defconst *is-win* (string-equal system-type "windows-nt"))
(defconst *is-app* (and (display-graphic-p) (not (daemonp))))
(defconst *is-server* (not (not (daemonp))))
(defconst *is-server-main* (string-equal "main" (daemonp)))
(defconst *is-server-coding* (string-equal "coding" (daemonp)))
(defconst *is-terminal* (not (or (display-graphic-p) (daemonp))))
(defconst *is-graphic* (or (display-graphic-p) (daemonp)))


;; setup work environment
(defvar emacs-work-enabled t)

(defvar my:use-ycmd nil)
(defvar my:use-ycmd-doc nil)

(defvar my:use-cpp-lsp t)

(defvar emacs-d
  (file-name-directory
   (file-chase-links load-file-name))
  "The giant turtle on which the world rests.")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; To avoid overloading the GNU Emacs custormization init.el file made
;; by the user with the UI, I add the generated code in a separate
;; file.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq-default custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file t))
(load custom-file 'noerror)

;; https://github.com/jwiegley/dot-emacs/blob/521a54eca1bb72279b36f11e08f6fb09ba728df4/init.el#L7
(defvar file-name-handler-alist-old file-name-handler-alist)

;; Portion of heap used for allocation.  Defaults to 0.1.
(setq gc-cons-percentage 0.6)

(setq package-enable-at-startup nil
      file-name-handler-alist nil
      message-log-max 16384
      gc-cons-percentage 0.6
      auto-window-vscroll nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; By default Emacs triggers garbage collection at ~0.8MB which makes
;; startup really slow. Since most systems have at least 64MB of memory,
;; we increase it during initialization.
(setq gc-cons-threshold (* 64 1000 1000))
(add-hook 'emacs-startup-hook
          `(lambda ()
             (setq file-name-handler-alist file-name-handler-alist-old
                   gc-cons-threshold 800000
                   gc-cons-percentage 0.1)
             (garbage-collect)) t)

;; Load newer version of .el and .elc if both are available
(setq load-prefer-newer t)

(require 'package)

;; Install use-package if not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; MELPA ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
  ;; (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)
  ;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)  
  )

;; https://github.com/kiwanami/emacs-epc/issues/35
;; remove cl deprecated warnings
(setq byte-compile-warnings '(cl-functions))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; use-package ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(eval-and-compile
  (setq use-package-verbose nil
        use-package-expand-minimally nil
        use-package-compute-statistics nil
        use-package-enable-imenu-support t
        debug-on-error nil))

(eval-when-compile
  (require 'use-package))

;; if you use :diminish
(use-package diminish
  :after use-package)

(use-package minions
  :config
  (setq minions-mode-line-lighter "⚙"
        minions-mode-line-delimiters (cons "" ""))
  (minions-mode 1))

;; https://github.com/tarsius/moody
;; replace mode line with tabs and ribons
(use-package moody
  :disabled 
  :config
  (setq x-underline-at-descent-line t)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode))


;; if you use any :bind variant
(use-package bind-key
  :after use-package)
;; (eval-and-compile
;;   (if init-file-debug
    ;; (setq use-package-verbose nil
    ;;       use-package-expand-minimally t)))

;; (require 'use-package-ensure)
;; (setq use-package-always-ensure t)


;; https://github.com/dholm/benchmark-init-el
(use-package benchmark-init
  :ensure t
  :config
  :disabled
  ;; To disable collection of benchmark data after init is done.
  ;; (add-hook 'after-init-hook 'benchmark-init/deactivate)
  (add-hook 'emacs-startup-hook 'benchmark-init/deactivate)
  )


(use-package emacs
  :ensure nil
  :delight
  (auto-fill-function " AF")
  (visual-line-mode)
  :init
  (put 'narrow-to-region 'disabled nil) ;; allow narrow-to-region
  (setq inhibit-startup-screen t
        initial-scratch-message nil
        sentence-end-double-space nil
        ring-bell-function 'ignore
        frame-resize-pixelwise t)
  ;; write over selected text on input... like all modern editors do
  (delete-selection-mode t)

  ;; use common convention for indentation by default
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 2)
  
  ;; Enable indentation+completion using the TAB key.
  ;; Completion is often bound to M-TAB.
  (setq tab-always-indent 'complete)
  )

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)
;;
;;
;;
(add-to-list 'load-path (locate-user-emacs-file "lisp/"))
(add-to-list 'load-path (locate-user-emacs-file "plugins/"))
(add-to-list 'load-path (locate-user-emacs-file "share/emacs/site-lisp"))
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/rtags")

;; Load my alias
(load "my-alias")

;; Always load newest byte code
(setq load-prefer-newer t)

;; Make the *scratch* buffer unkillable.
(use-package unkillable-scratch
  :ensure t
  :init (unkillable-scratch))

;; classic dired 
(use-package dired
  :config
  ;; dired - reuse current buffer by pressing 'a'
  (put 'dired-find-alternate-file 'disabled nil)

  ;; always delete and copy recursively
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always)

  ;; if there is a dired buffer displayed in the next window, use its
  ;; current subdir, instead of the current subdir of this dired buffer
  (setq dired-dwim-target t)

  ;; enable some really cool extensions like C-x C-j(dired-jump)
  (require 'dired-x))


(use-package desktop
  :disabled
  :init
  (desktop-save-mode 1)
  :config
  (setq desktop-restore-eager 20
        desktop-lazy-verbose nil
        desktop-base-file-name "desktop"
        desktop-base-lock-name "desktop.lock")
  (setq desktop-load-locked-desktop t)
  (add-to-list 'desktop-modes-not-to-save '(dired-mode fundamental-mode))
  (add-to-list 'desktop-globals-to-save 'tags-file-name)
  (setq desktop-buffers-not-to-save
        (concat "\\(" "^nn\\.a[0-9]+\\|\\.log\\|(ftp)\\|^tags\\|^TAGS"
	        "\\|\\.emacs.*\\|\\.diary\\|\\.newsrc-dribble\\|\\.bbdb" 
	        "\\)$"))
   (add-to-list 'desktop-modes-not-to-save 'dired-mode)
   (add-to-list 'desktop-modes-not-to-save 'Info-mode)
   (add-to-list 'desktop-modes-not-to-save 'info-lookup-mode)
   (add-to-list 'desktop-modes-not-to-save 'fundamental-mode)
   (add-to-list 'desktop-globals-to-save 'tags-file-name)
  )

(use-package find-file
  :bind ("C-c C-O" . ff-find-other-file)
  :config
  (setq cc-search-directories 
        (append cc-search-directories
                '("." ".." "../.."
                  "../src" "../../src"
                  "../inc" "../../inc"
                  "../../SOURCES"
                  "../include" "../../include")))  
  )

;; Better comments
;; I overwrite the build-in comment-dwim with its superior sequel.
(use-package comment-dwim-2
  :ensure t
  :bind ("M-;" . comment-dwim-2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; doxymacs
(use-package doxymacs
  :diminish doxymacs-mode
  :commands doxymacs-mode 
  :hook c-mode-common-hook
  :init
  (setq doxymacs-doxygen-style "C++")
  (setq doxymacs-command-character "\\")
  :config
  (unbind-key "C-c d" doxymacs-mode-map)
  :bind (([f12] . doxymacs-insert-function-comment)
         ([S-f12] . doxymacs-insert-member-comment))

  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;(require 'yasnippet-bundle) manap 19/08/2008
;; (require 'yasnippet)
;; (yas/global-mode 1)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package yasnippet
;;   :init
;;   (progn
;;     (use-package yasnippets))
;;   :catch (lambda (keyword err)
;;            (message (error-message-string err))))
;; ;;    (setq-default yas-prompt-functions '(yas-ido-prompt))))

;; (use-package yasnippet
;;   :init
;;   (yas-global-mode)
;;   :config
;;   (yas-reload-all))

;; (defvar emacs-d
;;   (file-name-directory
;;    (file-chase-links load-file-name))
;;   "The giant turtle on which the world rests.")

;; abo-abo approach
;; (use-package yasnippet
;;     :diminish yas-minor-mode
;;     :config
;;     (progn
;;       ;; (setq yas-after-exit-snippet-hook '((lambda () (yas-global-mode -1))))
;;       ;; (setq yas-fallback-behavior 'return-nil)
;;       (setq yas-triggers-in-field t)
;;       (setq yas-verbosity 0)
;; 	  (yas-global-mode 1)
;;       ;; (define-key yas-minor-mode-map [(tab)] nil)
;;       ;; (define-key yas-minor-mode-map (kbd "TAB") nil)
;; 	  ))
(use-package yasnippet
  :commands (yas-minor-mode) ; autoload `yasnippet' when `yas-minor-mode' is called
                                        ; using any means: via a hook or by user
                                        ; Feel free to add more commands to this
                                        ; list to suit your needs.
  :hook ((prog-mode . yas-minor-mode)
         (latex-mode . yas-minor-mode)
         (org-mode . yas-minor-mode))
  :diminish yas-minor-mode
  :config ; stuff to do after requiring the package
  (progn
    (yas-reload-all)
    (setq yas-verbosity 0)
    (setq yas-snippet-dirs (list (concat emacs-d "snippets/")))
    )
  ;; Add snippet support to lsp mode
  (setq lsp-enable-snippet t)
  )

(use-package auto-yasnippet
    :commands aya-create aya-open-line)


;; (yas/initialize)			
;;(yas/load-directory (expand-file-name "snippets" dotfiles-dir))
;;(yas/load-directory "~/.emacs.d/plugins/snippets")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package goto-last-change
;;   :bind([f4] . goto-last-change)
;;   )
(use-package goto-chg
  :bind ([f4] . goto-last-change)
  ;; ("M-." . goto-last-change-reverse))
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(global-set-key "\M-g" 'goto-line)
(display-time)                          ; Display the time on modeline
(line-number-mode t)                    ; Display the line number on modeline
(column-number-mode t)                  ; Display the column number on modeline
(setq-default kill-whole-line t)        ; Ctrl-k kills whole line if at col 0
;;(setq-default fill-column 75)         ; Wrap at col 75
(setq-default tab-width 2)              ; Show tabs as 8 cols
;;;;(setq-default transient-mark-mode t)    ; Turn on transient mark mode
(setq inhibit-startup-message t)        ; I'm sick of the welcome message

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package rg
  :ensure t
  :hook (after-init . rg-enable-default-bindings)
  :config
  (setq rg-group-result t)
  (setq rg-show-columns t)
  )

;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;; find other file
;;(global-set-key "\M-`" 'ff-find-other-file)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; global key bindings
(global-set-key (kbd "C-x C-z") nil)
(global-set-key [C-delete]    'kill-word)
(global-set-key [C-backspace] 'backward-kill-word)
(global-set-key [home]        'beginning-of-line)
(global-set-key [end]         'end-of-line)
(global-set-key [C-home]      'beginning-of-buffer)
(global-set-key [C-end]       'end-of-buffer)
(global-set-key [f1]          'find-file)
(global-set-key [S-f1]        'find-name-dired)
(global-set-key [S-f5]          'revert-buffer)
(global-set-key [S-f7]        'compile)
(global-set-key [f7]        'recompile)
(global-set-key [f8]          'next-error)
(global-set-key [S-f8]          'previous-error)
;; (global-set-key [S-right]     'bs-cycle-next)
;; (global-set-key [S-left]      'bs-cycle-previous)
(global-set-key [f9]          'save-buffer)
(global-set-key [f10]          'kill-buffer)
;;;
(global-set-key "\C-z" 'undo)
;; (global-set-key (kbd "C-c o") 'occur)
;; (global-set-key [S-f6] 'multi-occur)
;;;;; Turn on mouse wheel
(mouse-wheel-mode t)
;;;
;;;
;;;;;Delete selection mode
(delete-selection-mode 1)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Modern cpp font lock  ;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package modern-cpp-font-lock
  :ensure t
  :commands modern-c++-font-lock-mode
  :init (add-hook 'c++-mode-hook 'modern-c++-font-lock-mode))

 
;;auto fill mode for all text buffers
(add-hook 'text-mode-hook 'turn-on-auto-fill)
 
;;autofill toggle
(global-set-key (kbd "C-c q") 'auto-fill-mode)
 
;;============================================================================
;; ibuffer
;; organize buffer lists
;; http://martinowen.net/blog/2010/02/03/tips-for-emacs-ibuffer.html
;;==============================================================================
(use-package ibuffer
  :ensure t
  :bind ("C-x C-b" . ibuffer)
  :config
  (progn
    (use-package ibuffer-vc :ensure t)
    (use-package ibuffer-git :ensure t)
    (setq
     ibuffer-show-empty-filter-groups nil
     ibuffer-marked-char ?✓))
  (define-ibuffer-column size-h
    (:name "Size" :inline t)
    (cond
     ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
     ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
     (t (format "%8d" (buffer-size)))))
  (setq ibuffer-saved-filter-groups
        (quote (("default"
                 ("Programming" ;; prog stuff not already in MyProjectX
                  (or
                   (mode . c-mode)
                   (mode . c++-mode)
                   (mode . python-mode)
                   (mode . emacs-lisp-mode)
                   (mode . lisp-mode)
                   (mode . sql-mode)
                   (mode . html-mode)
                   (mode . js2-mode)
                   (mode . pascal-mode)
                   (mode . makefile-gmake-mode)
                   (mode . nxml-mode)
                   (mode . yaml-mode)
                   (mode . sh-mode)
                   (mode . rst-mode)
                   (mode . rust-mode)
                   (mode . go-mode)
                   (mode . po-mode)
                   ;; etc
                   ))
                 ("Dired"
                  (or
                   (mode . dired-mode)))
                 ("Makefile"
                  (or
                   (mode . make-mode)
                   (mode . cmake-mode)))
                 ("Version Control"
                  (or
                   (mode . magit-mode)
                   (name . "^*magit")
                   (mode . ahg-status-mode)))
                 ("Org" ;; all org-related buffers
                  (or
                   (mode . org-mode)
                   (mode . org-agenda-mode)
                   (mode . diary-mode)
                   (mode . calendar-mode)
                   (name . "^*Fancy Diary")
                   ))
                 ("Emacs"
                  (or
                   (name . "^\\*scratch\\*$")
                   (name . "^\\*Messages\\*$")
                   (name . "^\\*ielm\\*$")
                   (mode . help-mode)))
                 ("Gnus"
                  (or
                   (mode . message-mode)
                   (mode . bbdb-mode)
                   (mode . mail-mode)
                   (mode . gnus-group-mode)
                   (mode . gnus-summary-mode)
                   (mode . gnus-article-mode)
                   (name . "^\\.bbdb$")
                   (name . "^\\.newsrc-dribble")))
                 ))))
  )

(add-hook 'ibuffer-mode-hook
  (lambda ()
    (ibuffer-switch-to-saved-filter-groups "default")))

(use-package ibuffer-vc
  :after ibuffer
  :config
  (add-hook 'ibuffer-hook (lambda ()
                            (ibuffer-vc-set-filter-groups-by-vc-root)
                            (unless (eq ibuffer-sorting-mode 'filename/process)
                              (ibuffer-do-sort-by-filename/process)))))

;; duplicate-thing(duplicate line or duplicate selected range)
(use-package duplicate-thing
  :ensure t
  :commands (duplicate-thing)
  :bind ("C-c k" . duplicate-thing)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;IGREP;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(require 'igrep)
;;(global-set-key [f5]          'rgrep)
;;; ask for the default directory
;;(setq igrep-insert-default-directory t)

;;;;;;;;;;;;;;;TOOL-BAR;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(tool-bar-mode -1)    ;;disable the toolbars
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;; the blinking cursor is nothing, but an annoyance
(blink-cursor-mode -1)

;; disable the annoying bell ring
(setq ring-bell-function 'ignore)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (global-unset-key (kbd "C-z"))
;; (global-unset-key (kbd "C-c"))
(global-unset-key (kbd "C-x C-z"))

;;;;;;;;;;;;;;;DEFAULT font size;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (set-face-attribute 'default nil :height 100) 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;C++ mode for .h files ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; use C++ mode for .h files (instead of plain-old C mode)
(setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;YANG MODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'yang-mode "yang-mode" "Major mode for editing YANG modules." t)
(add-to-list 'auto-mode-alist '("\\.yang$" . yang-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;Browse kill buffer ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package browse-kill-ring
             :bind ("C-c y" . browse-kill-ring)
             :config
             (browse-kill-ring-default-keybindings))
;;;;;;;;;;;;;;;ABBREVIATIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;Pairing Mode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package electric-pair-mode
;;   :hook (prog-mode)
;;   :config
;;   (progn 
;;     (setq electric-pair-mode +1)
;;     (setq show-paren-mode 1))
;; )
;; some reference setup:
;; 1. https://github.com/mammothbane/emacs/blob/163e8ae3424469283bdb558532383e352b254b35/init.el#L123
;; 2. https://github.com/jowl/emacs.d/blob/0856c6b5d52a22fa97d8c262668516e3a037c680/packages.el#L68
(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :init
  (electric-pair-mode -1)
  :config 
  (require 'smartparens-config)
  (sp-use-smartparens-bindings)
  )

(use-package smartparens-config
  :ensure smartparens
  :config (show-smartparens-global-mode t)
  :hook ((prog-mode . smartparens-mode)
         (markdown-mode . smartparens-mode)
         (org-mode . smartparens-mode)
         )
  )

(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode-hook . rainbow-delimiters-mode)
)

;; see http://emacsrocks.com/e12.html
;; requires mark-multi available from ELPA
;; (require 'inline-string-rectangle)
;; (global-set-key (kbd "C-x r t") 'inline-string-rectangle)
(use-package multiple-cursors
  :ensure t
  :config
  (bind-keys :map global-map
	     ("C-<" . mc/mark-previous-like-this)
	     ("C->" . mc/mark-next-like-this)
	     ("C-M-m" . mc/mark-more-like-this)
	     ("C-M-*" . mc/mark-all-like-this)
         ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;HIGHLIGHT STUFF ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package highlight-symbol
  :ensure highlight-symbol
  :diminish highlight-symbol-mode
  :config
  (progn
    (setq highlight-symbol-on-navigation-p t)
    (setq highlight-symbol-idle-delay 0.75)
    (setq highlight-symbol-occurence-message '(explicit))

    (let ((f (lambda ()
	           (global-set-key (kbd "C-M-S-f") 'highlight-symbol-next)
	           (global-set-key (kbd "C-M-S-b") 'highlight-symbol-prev)
	           (highlight-symbol-mode 1)
	           )))
      (add-hook 'prog-mode-hook f)
      (add-hook 'text-mode-hook f)
      )
    
    )
  :bind
  ("C-*" . 'highlight-symbol-at-point)
  ;; (global-set-key [(meta f3)] 'highlight-symbol-query-replace)
  )

;; highlight the current line
(use-package hl-line
  :config
  (global-hl-line-mode +1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;JS STUFF;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(add-to-list 'load-path "~/.emacs.d/lintnode")
;;(require 'flymake-jslint)
;; Make sure we can find the lintnode executable
;;(setq lintnode-location "~/.emacs.d/lintnode")
;; JSLint can be... opinionated
;;(setq lintnode-jslint-excludes (list 'nomen 'undef 'plusplus 'onevar 'white))
;;(setq lintnode-jslint-set "maxlen:80")
;; Start the server when we first open a js file and start checking
;;(add-hook 'js-mode-hook
;;          (lambda ()
 ;;           (lintnode-hook)))

;;(add-hook 'js-mode-hook
;;          (lambda ()
            ;; Scan the file for nested code blocks
;;            (imenu-add-menubar-index)
            ;; Activate the folding mode
  ;;          (hs-minor-mode t)))

;; Nice Flymake minibuffer messages
;;(require 'flymake-cursor)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defconst my-c-style
  '((c-tab-always-indent        . t)
    (c-comment-only-line-offset . 4)
    (c-hanging-braces-alist     . ((substatement-open after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
    (c-cleanup-list             . (scope-operator
                                   empty-defun-braces
                                   defun-close-semi))
    (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
                                   (substatement-open . ++)
                                   (case-label        . 4)
                                   (block-open        . 0)
                                   (knr-argdecl-intro . -)))
    (c-echo-syntactic-information-p . t)
    )
  "My C Programming Style")

;; offset customizations not in my-c-style
;;;(setq c-offsets-alist '((member-init-intro . ++)))

;; Customizations for all modes in CC Mode.
(defun my-c-mode-common-hook ()
  ;; add my personal style and set it for the current buffer
  (c-add-style "PERSONAL" my-c-style t)
  ;; other customizations
  (setq tab-width 4
        ;; this will make sure spaces are used instead of tabs
        indent-tabs-mode nil)
  ;; we like auto-newline and hungry-delete
  (c-toggle-auto-hungry-state 1)
  ;; key bindings for all supported languages.  We can put these in
  ;; c-mode-base-map because c-mode-map, c++-mode-map, objc-mode-map,
  ;; java-mode-map, idl-mode-map, and pike-mode-map inherit from it.
  (define-key c-mode-base-map "\C-m" 'c-context-line-break)
  )

;;https://github.com/markcol/emacs.d/blob/4db26ca764c41bf9944ee9e9730c6543b4fff15a/init.el
;;https://github.com/nilsdeppe/MyEnvironment/blob/master/.emacs.el
(use-package cc-mode
  :ensure t
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.tpp\\'" . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.hpp\\'" . c++-mode))
  (add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
  :bind (:map c-mode-map
         ("C-c C-O" . ff-find-other-file)
         :map c++-mode-map
         ("C-c C-O" . ff-find-other-file))
  :preface
  (defun my/c-mode-hook ()
    (setq c-default-style '((java-mode . "java")
                            (awk-mode  . "awk")
                            (other     . "k&r")))
    ;; (c-set-offset 'statement-case-open 0)
    (c-set-style "k&r")
    (setq indent-tabs-mode nil)
    (setq c-basic-offset   2)
    (setq comment-column   40)
    (c-toggle-auto-hungry-state 1)
    (c-toggle-auto-newline 0))
  :config
  ;; (progn
  ;;   (add-hook 'c-mode-common-hook #'my/c-mode-hook))  
  (custom-set-variables '(c-noise-macro-names '("constexpr")))
  (use-package google-c-style
    :ensure t
    :config
    ;; This prevents the extra two spaces in a namespace that Emacs
    ;; otherwise wants to put... Gawd!
    (add-hook 'c-mode-common-hook 'google-set-c-style)
    ;; Autoindent using google style guide
    (add-hook 'c-mode-common-hook 'google-make-newline-indent)
    )
  )

;; don't use tabs 
(setq-default indent-tabs-mode nil)

(use-package abbrev
  :diminish abbrev-mode
  :defer t
  :config
  ;; do not bug me about saving my abbreviations
  (setq save-abbrevs nil)
  ;; ensure abbrev mode is always on
  (setq-default abbrev-mode t)
  ;; :hook ((text-mode . abbrev-mode)
  ;;        (git-commit-mode . abbrev-mode))
  )


;; load up modes I use
;; (require 'cc-mode)
;; cc-mode
(use-package cc-mode
  :commands (c-mode c++-mode objc-mode java-mode)
  :mode ("\\.mm" . objc-mode)
  :init
)
;;(require 'perl-mode)
;;(require 'cperl-mode)
;; (require 'sh-script)
;; (require 'shell)
;;(require 'tex-site) ;; I use AUCTeX
;;(require 'latex)    ;; needed to define LaTeX-mode-hook under AUCTeX
;;(require 'tex)      ;; needed to define TeX-mode-hook under AUCTeX
;; (require 'python)   ;; I use python.el from Emacs CVS, uncomment if you do also

;; load up abbrevs for these modes
;;(require 'msf-abbrev)
;;(setq msf-abbrev-verbose t) ;; optional
;;(setq msf-abbrev-root "~/emacs/mode-abbrevs")
;;(global-set-key (kbd "C-c l") 'msf-abbrev-goto-root)
;;(global-set-key (kbd "C-c a") 'msf-abbrev-define-new-abbrev-this-mode)
;;(msf-abbrev-load)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;ETags List ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'etags-select)
;; (global-set-key "\M-?" 'etags-select-find-tag-at-point)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;scroll bar ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(set-scroll-bar-mode 'left)
(setq
  scroll-margin 0                  
  scroll-conservatively 100000
  scroll-preserve-screen-position 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;; uniquify ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; make buffers name unique
(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward
        uniquify-separator "/"
        uniquify-after-kill-buffer-p t
        uniquify-ignore-buffers-re "^\\*"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;; split width;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq split-width-threshold most-positive-fixnum)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;; clang format;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (load "/home/manap/Projects/LLVM/llvm/tools/clang/tools/clang-format/clang-format.el")
;; (require 'clang-format)
;; (setq style "Google")
;; (custom-set-variables)
;; (global-set-key [f2] 'clang-format-region)
(use-package clang-format
	     :ensure t
             :bind (([f2] . clang-format-region))
             )

;; (setq default-process-coding-system '(undecided-dos . undecided-dos))
;; (setq default-process-coding-system '(undecided-unix . undecided-unix))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;; cmake mode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package cmake-mode
  :ensure t
  :mode (("CMakeLists\\.txt\\'" . cmake-mode)
         ("\\.cmake\\'" . cmake-mode))
  )

(use-package cmake-font-lock
  :ensure t
  :hook (cmake-mode . cmake-font-lock-activate))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;; iseach mode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun isearch-yank-regexp (regexp)
  "Pull REGEXP into search regexp." 
  (let ((isearch-regexp nil)) ;; Dynamic binding of global.
	(isearch-yank-string regexp))
  (isearch-search-and-update))

(defun isearch-yank-symbol (&optional partialp)
  "Put symbol at current point into search string.

  If PARTIALP is non-nil, find all partial matches."
  (interactive "P")
  (let* ((sym (find-tag-default))
		 ;; Use call of `re-search-forward' by `find-tag-default' to
		 ;; retrieve the end point of the symbol.
		 (sym-end (match-end 0))
		 (sym-start (- sym-end (length sym))))
	(if (null sym)
		(message "No symbol at point")
	  (goto-char sym-start)
	  ;; For consistent behavior, restart Isearch from starting point
	  ;; (or end point if using `isearch-backward') of symbol.
	  (isearch-search)
	  (if partialp
		  (isearch-yank-string sym)
		(isearch-yank-regexp
		 (concat "\\<" (regexp-quote sym) "\\>"))))))

(defun isearch-current-symbol (&optional partialp)
  "Incremental search forward with symbol under point.

  Prefixed with \\[universal-argument] will find all partial
  matches."
  (interactive "P")
  (let ((start (point)))
	(isearch-forward-regexp nil 1)
	(isearch-yank-symbol partialp)))

(defun isearch-backward-current-symbol (&optional partialp)
  "Incremental search backward with symbol under point.

  Prefixed with \\[universal-argument] will find all partial
  matches."
  (interactive "P")
  (let ((start (point)))
	(isearch-backward-regexp nil 1)
	(isearch-yank-symbol partialp)))

(global-set-key [f3] 'isearch-forward-symbol-at-point)
(global-set-key [(meta f3)] 'isearch-backward-current-symbol)

;; Subsequent hitting of the keys will increment to the next
;; match--duplicating `C-s' and `C-r', respectively.
(define-key isearch-mode-map [f3] 'isearch-repeat-forward)
(define-key isearch-mode-map [(meta f3)] 'isearch-repeat-backward)
; Activate occur easily inside isearch
(when (fboundp 'isearch-occur)
    ;; to match ivy conventions
  (define-key isearch-mode-map (kbd "C-c C-o") 'isearch-occur))

(use-package anzu
  :ensure t
  :diminish anzu-mode
  :init
  (global-anzu-mode))

;; move-line is taken from here:
;; http://www.emacswiki.org/emacs/MoveLine
(defun move-line (n)
  "Move the current line up or down by N lines."
  (interactive "p")
  (setq col (current-column))
  (beginning-of-line) (setq start (point))
  (end-of-line) (forward-char) (setq end (point))
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (insert line-text)
    ;; restore point to original column in moved line
    (forward-line -1)
    (forward-char col)))

(defun move-line-up (n)
  "Move the current line up by N lines."
  (interactive "p")
  (move-line (if (null n) -1 (- n))))

(defun move-line-down (n)
  "Move the current line down by N lines."
  (interactive "p")
  (move-line (if (null n) 1 n)))

(global-set-key (kbd "M-<up>") 'move-line-up)
(global-set-key (kbd "M-<down>") 'move-line-down)



;; Move line or region up or down
;; Grabbed von SO http://stackoverflow.com/questions/2423834/move-line-region-up-and-down-in-emacs
(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(global-set-key [M-up] 'move-text-up)
(global-set-key [M-down] 'move-text-down)

;; whole line or region
(use-package whole-line-or-region
  :ensure t
  :config (whole-line-or-region-global-mode t)
  :diminish whole-line-or-region-global-mode
  )

;; ;; news ticker
;; ;;

;; (require 'newsticker)

;; ; W3M HTML renderer isn't essential, but it's pretty useful.
;; (require 'w3m)
;; (setq newsticker-html-renderer 'w3m-region)
;; ;
;; ; ; We want our feeds pulled every 10 minutes.
;; (setq newsticker-retrieval-interval 600)
;; ;
;; ; ; Setup the feeds. We'll have a look at these in just a second.
;; (setq newsticker-url-list-defaults nil)
;; ;;(setq newsticker-url-list '("."))
;; ;
;; ; ; Optionally bind a shortcut for your new RSS reader.
;; (global-set-key (kbd "C-c r") 'newsticker-treeview)

;; (add-hook 'newsticker-mode-hook 'imenu-add-menubar-index)
;; ;;(setq newsticker-url-list
;; ;;  (google-reader-to-newsticker "/home/manap/.emacs.d/to/subscriptions.xml"))

;; ;
;; ; ; Don't forget to start it!
;; (newsticker-start)

(if emacs-work-enabled
    (progn ; Lets you evaluate more than one sexp for the true case
      ;; (ding)
      (message "Work setup, not very lucky")
      (setq my:thirdparty_path (expand-file-name "/projects/Thirdparty/"))
      (setq my:org-agenda-files (list "~/Documents/notes/work/todo.org"))
      (setq my:org-agenda-todo (expand-file-name "~/Documents/notes/work/todo.org"))
      (setq my:org-refile-targets '(("~/Documents/notes/work/todo.org" :level . 1)
                                    ))
      )
  (setq my:thirdparty_path (expand-file-name "~/Project/Thirdparty/YouCompleteMe/third_party/"))
  (setq my:org-agenda-files (list "~/Dropbox/Personal/coffee/daily.org"))
  (setq my:org-agenda-todo (expand-file-name "~/Dropbox/Personal/coffee/daily.org"))
  (setq my:org-refile-targets '(("~/Dropbox/Personal/coffee/daily.org" :maxlevel . 2)
                                ("~/Dropbox/Personal/coffee/someday.org" :level . 2)
                                )) 
  ;; (ding)
  (message "Home setup, lucky you.")
  )
  
(message "path %s" my:thirdparty_path)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Projectile ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package projectile
  :defer t
  :diminish projectile-mode
  :commands (projectile-global-mode
             projectile-ignored-projects
             projectile-compile-project
             projectile-find-file-in-directory)
  :init
  (setq projectile-mode-line nil)
  (projectile-global-mode)
  :config
  (setq projectile-enable-caching nil)
  (setq projectile-indexing-method 'alien)
  (setq projectile-completion-system 'ivy)
  ;; global ignores:
  (add-to-list 'projectile-globally-ignored-files "node_modules")
  (add-to-list 'projectile-globally-ignored-files ".tern-port")
  (add-to-list 'projectile-globally-ignored-files ".DS_Store")
  (add-to-list 'projectile-globally-ignored-files ".clangd")
  (setq projectile-globally-ignored-buffers '("*eshell*"
                                              "*magit-process*"
                                              "TAGS"))
  (setq projectile-verbose nil)
  (setq projectile-do-log nil)
  ;; always ignore .class files:
  (setq projectile-globally-ignored-file-suffixes 
        '(;; elisp
	      "elc"
	      ;; python
	      "pyc"
	      ;; general documentations
	      ;; "txt"
	      "rst" "doc" "html" "qdoc" "log" "mft" "ver" 
	      "png" "jpg" "bmp" "tif"
	      ;; binary
	      "dll" "lib" "exe"
	      ;; obj file
	      ".obj" ".o" ".so" ".a" ".P" ".d" ".D"
	      ;; unity
	      ".meta"
          "~" "#"
	      ))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; recentf  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package recentf
  :hook (after-init . recentf-mode)
  :config
;;  (setq recentf-save-file (expand-file-name "recentf" bozhidar-savefile-dir)
  (setq recentf-max-saved-items 100
        recentf-max-menu-items 15
        ;; disable recentf-cleanup on Emacs start, because it can cause
        ;; problems with remote files
        recentf-auto-cleanup 'never)
  (recentf-mode +1)
  (setq recentf-exclude '((expand-file-name package-user-dir)
                     ".cache"
                     "cache"
                     "recentf"
                     "bookmarks"
                     "undo-tree-hist"
                     "url"
                     "COMMIT_EDITMSG\\'"))
  (add-to-list 'recentf-exclude
               (format "%s/\\.emacs\\.d/elpa/.*" (getenv "HOME")))
  (add-to-list 'recentf-exclude
               (format "/projects/emacs/dot-emacs/\\.emacs\\.d/elpa/.*" (getenv "HOME")))
  (add-to-list 'recentf-exclude
               (format "%s/\\.emacs\\.d/\\(ido\\.last\\|recentf\\|\\.gitignore\\)" (getenv "HOME")))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; ace-jump-mode  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package avy
  :requires hydra
  :config
  (defhydra hydra-avy (:exit t :hint nil)
    "
 Line^^       Region^^        Goto
----------------------------------------------------------
 [_y_] yank   [_Y_] yank      [_c_] timed char
 [_m_] move   [_M_] move      [_w_] word
 [_k_] kill   [_K_] kill      [_l_] line        [_L_] end of line"
    ("c" avy-goto-char)
    ("w" avy-goto-word-1)
    ("l" avy-goto-line)
    ("L" avy-goto-end-of-line)
    ("m" avy-move-line)
    ("M" avy-move-region)
    ("k" avy-kill-whole-line)
    ("K" avy-kill-region)
    ("y" avy-copy-line)
    ("Y" avy-copy-region)
    )
  :bind (("C-:" . avy-goto-char)
         ("C-\"" . avy-goto-char-timer)
         ("C-c h a" .  hydra-avy/body)
         )  
 )

(use-package ace-link
  :config (ace-link-setup-default))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; whitespace-mode  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package whitespace
  :bind ("C-c w" . whitespace-mode)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Subword for programming mode only 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package subword
  :diminish ""
  :hook (prog-mode . subword-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Expand Region 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package expand-region
  :ensure t
  :defer t
  :bind ("M-2" . er/expand-region)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helm imenu
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package helm
  :config
  (setq helm-imenu-fuzzy-match    t)
  )
;; (require 'helm)
;;(semantic-mode 1)
;;(setq helm-semantic-fuzzy-match t
 ;;     helm-imenu-fuzzy-match    t)

(defun as-helm-org-rifle-notes ()
  (interactive)
  (helm-org-rifle-directories '("~/Documents/notes")))

(use-package helm-org-rifle
  :ensure t
  :after helm
  :bind (("C-c h r c" . helm-org-rifle-current-buffer)
         ("C-c h r a" . helm-org-rifle)
         ("C-c h r n" . as-helm-org-rifle-notes)
         ))

(use-package helm-dash
  :config
  ;; Key Bindings
  (bind-key "C-c d" #'helm-dash-at-point)
  ;; Variables
  (setq helm-dash-common-docsets '("C++" "Python 2" "Python 3" "Rust" "CMake"))
  (setq helm-dash-enable-debugging nil)
  (setq helm-dash-docsets-path "~/.docsets/")
  (setq helm-dash-browser-func 'eww)
  (setq helm-dash-min-length 2))
;;(global-set-key "\C-\c\h\i" 'helm-semantic-or-imenu)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Volatile Highlight
;; https://github.com/k-talo/volatile-highlights.el
;; brings feedback by highlighting some portions related to operations
(use-package volatile-highlights
  :diminish volatile-highlights-mode
  :init (volatile-highlights-mode t))

(use-package hl-line+
  :ensure t
  :disabled
  :config
  (global-hl-line-mode)
  (setq font-lock-maximum-decoration t)
  (setq font-lock-maximum-size nil)
  (global-font-lock-mode t)
  (transient-mark-mode t))

;;
;; visual bookmarks
;;
(use-package bm
  :ensure
  :config
  (setq bm-annotate-on-create nil)
  (setq bm-buffer-persistence t)
  (setq bm-cycle-all-buffers t)
  (setq bm-goto-position nil)
  (setq bm-highlight-style 'bm-highlight-line-and-fringe)
  (setq bm-in-lifo-order t)
  (setq bm-recenter t)
  (setq bm-repository-file "~/.emacs.d/bm-bookmarks")
  (setq bm-repository-size 100)
  (setq bm-show-annotations t)
  (setq bm-wrap-immediately t)
  (setq bm-wrap-search t)
  ;; :bind (("<C-f8>" . bm-next)
  ;;        ("<C-S-f8>" . bm-previous)
  ;;        ("<s-f8>" . bm-toggle-buffer-persistence)
  ;;        ("<f8>" . bm-toggle))
  (defhydra my-hydra/bm (global-map "C-.")
    "For bm.el use."
    ("t" bm-toggle)
    ("n" bm-next)
    ("p" bm-previous)
    )
  )

(use-package wgrep
  :ensure t
  )

;; offers the ability to move to the first or last actionable point in
;; a buffer rather than the absolute maximum or minimum point.
(use-package beginend
  :ensure
  :demand
  :diminish beginend-global-mode
  :config
  (dolist (mode beginend-modes) (diminish (cdr mode)))
  (beginend-global-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  18.3.4 Column/line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package nlinum
  :ensure t
  :disabled t
  :config
  (line-number-mode t)
  (column-number-mode t)
  (global-nlinum-mode t)
  (size-indication-mode t))

(use-package nlinum-hl
  :ensure t
  :after nlinum
  :disabled t
  :config
  (add-hook 'nlinum-mode-hook (lambda () (setq nlinum-highlight-current-line t))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rtags with imenu and helm
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://github.com/gustavw/dot_emacs/blob/15b4b68ab617ff3ebfa43b0616c0e33d3d3e856f/custom/setup-rtags.el
(use-package rtags
  :ensure t
  :after (cc-mode)
  :init
  ;; (add-hook 'c-mode-hook 'rtags-start-process-unless-running)
  ;; (add-hook 'c++-mode-hook 'rtags-start-process-unless-running)
  ;; (add-hook 'objc-mode-hook 'rtags-start-process-unless-running)
  (rtags-enable-standard-keybindings)
  :config
  (use-package helm-rtags :ensure t)
  ;;(rtags-start-process-unless-running)
  (setq rtags-autostart-diagnostics t)
  (rtags-diagnostics)
  ;;(setq rtags-completions-enabled t)
  (setq rtags-use-helm t)
  :bind (([C-f5] . rtags-find-symbol)
         ([C-f6] . rtags-find-references-at-point)
         ([C-f7] . rtags-location-stack-back)
         ([C-f8] . rtags-next-match)
         ([C-f9] . rtags-references-tree))
)

;;(require 'rtags)
;;(require 'helm-rtags)
;;(rtags-enable-standard-keybindings)
;;(setq rtags-use-helm t)

(use-package helm-imenu
  :ensure nil
  :requires helm imenu
  :defer t
  :config
  (setq helm-imenu-fuzzy-match    t)
  )

(use-package imenu
  :ensure t
  :defer t
  :bind ("C-c h i" . helm-imenu)
  :init
  (autoload 'helm-imenu "imenu" nil t))

;;;;
;;;; highlight leading spaces
;;;; 
;;(use-package highlight-leading-spaces
;;  :ensure t
;;  :defer t
;;  :init (add-hook 'prog-mode-hook 'highlight-leading-spaces-mode))

;; dabbrev 
(use-package dabbrev
  :after minibuffer ; read those as well
  :config
  (setq dabbrev-abbrev-char-regexp "\\sw\\|\\s_")
  (setq dabbrev-abbrev-skip-leading-regexp "\\$\\|\\*\\|/\\|=")
  (setq dabbrev-backward-only nil)
  (setq dabbrev-case-distinction nil)
  (setq dabbrev-case-fold-search t)
  (setq dabbrev-case-replace nil)
  (setq dabbrev-check-other-buffers t)
  (setq dabbrev-eliminate-newlines nil)
  (setq dabbrev-upcase-means-case-search t)
  :bind (("M-/" . dabbrev-expand)
         ("C-M-/" . dabbrev-completion)
         )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; YouCompleteMe
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Backquote.html
;; `(xxxx ,(expand-file-name yyy))
(setq my:main-py-path (format "%s/%s" my:thirdparty_path "ycmd/ycmd/__main__.py"))
(setq my:global-py-path (format "%s/%s" my:thirdparty_path "ycmd/ycm_global_conf.py"))
(defvar my:ycmd-server-command (list "python"
                                     (expand-file-name my:main-py-path))) 
(defvar my:ycmd-global-config (expand-file-name my:global-py-path))
(defvar my:ycmd-extra-conf-whitelist  '("~/Projects/xpu/32nms/*" "~/Projects/Thirdparty/LLVM/*" "/projects/icom/mpls/zebos/*"))
(defvar my:python-location (executable-find (nth 0 my:ycmd-server-command)))

(message "setup ycmd")

(use-package ycmd
  :if my:use-ycmd
  :ensure t
  :defer t
  :init
  ;;(add-hook 'after-init-hook #'global-ycmd-mode)
;;  (add-hook 'c++-mode-hook 'ycmd-mode)
  (message "loading ycmd")
  :hook
  (c++-mode . ycmd-mode)
  :config
  (progn
    (set-variable 'ycmd-server-command my:ycmd-server-command)
    (set-variable 'ycmd-extra-conf-whitelist my:ycmd-extra-conf-whitelist)
    (set-variable 'ycmd-global-config my:ycmd-global-config)
    ;; need to silence the error in *ycmd-server* buffer and probably
    ;; the --log=error makes the difference.
    ;; (setq ycmd-server-args '("--log=error" "--keep_logfile" "--idle_suicide_seconds=10800"))
    (setq ycmd-server-args '("--log=info" "--keep_logfile" "--idle_suicide_seconds=10800"))
;;	(setq ycmd-global-modes '(not emacs-lisp-mode org-mode helm-mode))
    (setq ycmd-force-semantic-completion t)
	(setq ycmd-parse-conditions '(save new-line mode-enabled idle-change))
    

    (use-package company-ycmd
      :ensure t
      :config
      (company-ycmd-setup)
      (message "loading company-ycmd")
      )

    (use-package flycheck-ycmd
      :ensure t
      :init
      (add-hook 'c-mode-common-hook 'flycheck-ycmd-setup)
      (message "loading flycheck-ycmd")
      :config
      ;; (flycheck-ycmd-setup)
      (when (not (display-graphic-p))
        (setq flycheck-indication-mode nil))
      )

    ;; Add displaying the function arguments in mini buffer using El Doc
    (use-package ycmd-eldoc
      :if my:use-ycmd-doc
      :init
      (message "loading ycmd-eldoc")
      ;; For some reason ycmd-eldoc doesn't work properly in Python mode.
      ;; I get a "connection refused" error when it connects to JediHTTP
      (add-hook 'c-mode-common-hook
                (lambda ()
                  (ycmd-eldoc-mode t)))
      )
    )
  )

;; old working 
;; (require 'ycmd)
;; (require 'company-ycmd)
;; (require 'flycheck-ycmd)

;; (company-ycmd-setup)
;; (flycheck-ycmd-setup)

;; Show completions after 0.15 seconds
(setq company-idle-delay 0.15)

;; Activate for editing C++ files
;;(add-hook 'c++-mode-hook 'ycmd-mode)
;;(add-hook 'c++-mode-hook 'company-mode)
;(add-hook 'c++-mode-hook 'flycheck-mode)

;; Replace the directory information with where you downloaded ycmd to
;; (set-variable 'ycmd-server-command (list "python" (substitute-in-file-name "$HOME/dev/ycmd/ycmd/__main__.py")))
;; (set-variable 'ycmd-server-command '("python" "/home/amanol/Project/Thirdparty/YouCompleteMe/third_party/ycmd/ycmd/__main__.py"))

;; Edit according to where you have your Chromium/Blink checkout
;; (add-to-list 'ycmd-extra-conf-whitelist (substitute-in-file-name "/home/amanol/Project/icom/omniparser/32nms/.ycm_extra_conf.py"))

;; Show flycheck errors in idle-mode as well
;; (setq ycmd-parse-conditions '(save new-line mode-enabled idle-change))

;; Makes emacs-ycmd less verbose
(setq url-show-status nil)

;; Set global conf
;; (set-variable 'ycmd-global-config "/home/amanol/Project/Thirdparty/YouCompleteMe/ycm_global_conf.py")

;;(set-variable 'ycmd-extra-conf-whitelist '("~/Projects/xpu/32nms/*" "~/Projects/Thirdparty/LLVM/*"))

;; ; Add yasnippet support for all company backends
;; ;; https://github.com/syl20bnr/spacemacs/pull/179
;; (defvar company-mode/enable-yas t
;;   "Enable yasnippet for all backends.")

;; (setq ycmd--log-enabled t)
;; Add yasnippet support for all company backends
;; https://github.com/syl20bnr/spacemacs/pull/179
(defvar company-mode/enable-yas t
  "Enable yasnippet for all backends.")
;; (defun company-mode/backend-with-yas (backend)
;;   (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
;;       backend
;;     (append (if (consp backend) backend (list backend))
;;             '(:with company-yasnippet))))

;; (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Validation of setq and stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package validate
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Company mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package company
  :defer 10
  :diminish company-mode
  :hook (c++-mode . company-mode)
  :bind (:map company-active-map
              ("M-j" . company-select-next)
              ("M-k" . company-select-previous))
  :preface
  ;; enable yasnippet everywhere
  (defvar company-mode/enable-yas t
    "Enable yasnippet for all backends.")
  (defun company-mode/backend-with-yas (backend)
    (if (or 
         (not company-mode/enable-yas) 
         (and (listp backend) (member 'company-yasnippet backend)))
        backend
      (append (if (consp backend) backend (list backend))
              '(:with company-yasnippet))))

  :init (global-company-mode t)
  :config
  ;; no delay no autocomplete
  (validate-setq
   company-idle-delay 0.15
   company-minimum-prefix-length 2
   company-tooltip-limit 20)
  (setq company-backends (delete 'company-semantic company-backends))
  (setq company-backends (delete 'company-eclim company-backends))
  (setq company-backends (delete 'company-xcode company-backends))
  (setq company-backends (delete 'company-clang company-backends))
;;  (setq company-backends (delete 'company-bbdb company-backends))
  (setq company-backends (delete 'company-oddmuse company-backends))
  (validate-setq company-backends 
                 (mapcar #'company-mode/backend-with-yas company-backends))
  (when my:use-ycmd
  (add-to-list 'company-backends (company-mode/backend-with-yas 'company-ycmd))
  )
  )


(use-package company-cmake
  :ensure t
  :disabled
  :after (cc-mode company)
  :config (add-to-list 'company-backends 'company-cmake))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Show argument list in echo area
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package eldoc
  :diminish eldoc-mode
  :init (add-hook 'ycmd-mode-hook 'ycmd-eldoc-setup))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Plantuml
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq plantuml-jar-path "/home/manap/Downloads/plantuml.jar") 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Zeal at point for offline documentation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package zeal-at-point
  :commands (zeal-at-point zeal-at-point-with-docset)
  :bind ("C-c d" . zeal-at-point)
  :config
  (progn
    (add-to-list 'zeal-at-point-mode-alist '(sh-mode . "bash"))
    (add-to-list 'zeal-at-point-mode-alist '(python-mode . "py3"))
    (add-to-list 'zeal-at-point-mode-alist '(c++-mode . "c++"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rust support
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; lsp-mode setup https://www.mortens.dev/blog/emacs-and-the-language-server-protocol/
;; https://github.com/MatthewZMD/.emacs.d#lsp-mode
(use-package lsp-mode
  :commands lsp
  :if *is-app*
  :custom
  (lsp-auto-guess-root nil)
  (lsp-prefer-flymake nil) ; Prefer using lsp-ui (flycheck) over flymake.
  (lsp-file-watch-threshold 2000)
  (read-process-output-max (* 1024 1024))
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :hook (((python-mode rust-mode objc-mode) . lsp)
         (lsp-mode . lsp-enable-which-key-integration)
         )
  :init
  ;; Disable yasnippet. We re-enable when yasnippet is loaded.
  (defvar lsp-enable-snippet nil)
  ;; use c++ lsp only on my:use-cpp-lsp
  (when my:use-cpp-lsp
    (add-hook 'c-mode-common-hook #'lsp)
  )
  (message "loading lsp-mode")
  :config
  ;; Set GC threshold to 25MB since LSP mode is very memory hungry and
  ;; produces a lot of garbage
  (setq gc-cons-threshold 25000000)
  (setq lsp-enable-on-type-formatting nil)
  (lsp-register-client
   (make-lsp-client
    :new-connection (lsp-tramp-connection "clangd")
    :major-modes '(c++-mode c-mode)
    :ignore-messages nil
    :remote? t
    :server-id 'c++-remote
    ))
  )

(use-package lsp-ivy
  :after lsp
  :bind ("s-l l f" . lsp-ivy-workspace-symbol)
  :commands ivy-lsp-workspace-symbol)

 (use-package tree-sitter-langs
  :ensure t
  :defer t)

(use-package tree-sitter
  :ensure t
  :after tree-sitter-langs
  :config
  (global-tree-sitter-mode))


(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm
  (setq-local buffer-save-without-query t))


(use-package rust-mode
  :disabled
  :ensure t
  :init
  (add-hook 'rust-mode-hook
	    (lambda () (local-set-key (kbd "C-c f") #'rust-format-buffer)))
  :config
  ;; (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'rust-mode-hook 'company-mode)
)

(use-package cargo
  :ensure t
  :defer t
  :after rust-mode
  :config
  (add-hook 'rust-mode-hook #'cargo-minor-mode)
  )

(use-package lsp-ui
  :requires lsp-mode flycheck
  :hook (lsp-mode . lsp-ui-mode)
  :bind (:map lsp-ui-mode-map
              ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
              ([remap xref-find-references] . lsp-ui-peek-find-references))
  :config
  (setq lsp-ui-doc-enable t
        lsp-ui-doc-use-childframe t
        lsp-ui-doc-position 'top
        lsp-ui-doc-include-signature t
        lsp-ui-sideline-enable nil
        lsp-ui-flycheck-enable t
        lsp-ui-flycheck-list-position 'right
        lsp-ui-flycheck-live-reporting t
        lsp-ui-peek-enable t
        lsp-ui-peek-list-width 60
        lsp-ui-peek-peek-height 25)

  )

(use-package company-lsp  
  :defer
  :requires company 
  :commands (company-lsp)
  :custom
  (company-lsp-async t)
  :config
  (push 'company-lsp company-backends))
;; (use-package racer
;;   :defer t
;;   :ensure t
;;   :after rust-mode
;;   :diminish racer-mode
;;   :config
;;   (add-hook 'rust-mode-hook #'racer-mode)
;;   (add-hook 'racer-mode-hook #'company-mode)
;;   (add-hook 'racer-mode-hook #'eldoc-mode)
;;   (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
;;   (setq company-tooltip-align-annotations t)
;; )

;; (use-package flycheck-rust
;;   :defer t
;;   :ensure t
;;   :after racer
;;   :config
;;   (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
;;   )

;;(require 'company)
;;(require 'racer)
;;(require 'rust-mode)
;; (require 'electric)
;;(require 'eldoc)
;;(require 'flycheck)
;; (use-package flycheck-rust :ensure t)
;;(require 'flycheck-rust)
;; init company for rust
;; (setq company-tooltip-align-annotations t)
;; (add-hook 'rust-mode-hook 'company-mode)

;; cargo hook
;;(add-hook 'rust-mode-hook 'cargo-minor-mode)

;; (add-hook 'rust-mode-hook
;;           (lambda ()
;;             (local-set-key (kbd "f2") #'rust-format-buffer)))

;; racer
;; (setq racer-cmd (concat (getenv "HOME")"/.cargo/bin/racer")) ;; Rustup binaries PATH
;; (setq racer-rust-src-path (concat (getenv "HOME") "/Project/Thirdparty/rust/src")) ;; Rust source code PATH

;; (setq racer-cmd "~/.cargo/bin/racer") ;; Rustup binaries PATH
;; (setq racer-rust-src-path (concat (getenv "HOME") "/Project/Thirdparty/rust/src")) ;; Rust source code PATH

;; ;;(add-hook 'rust-mode-hook #'racer-mode)
;; (add-hook 'racer-mode-hook #'eldoc-mode)
;; (add-hook 'racer-mode-hook #'company-mode)

;; flycheck
;; https://github.com/tomjakubowski/.emacs.d/blob/master/init.el
;; (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
(use-package flycheck
  :defer t
  :hook (c++-mode . flycheck-mode)
  ;; :config
  ;; (progn
  ;;   (add-hook 'flycheck-mode-hook 'flycheck-rust-setup))
  )

;; Plantuml path
(setq plantuml-jar-path (expand-file-name "~/Downloads/plantuml.jar"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Magit magic 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package magit
  :init
  (progn
    (setq magit-last-seen-setup-instructions "1.4.0"))
  :bind ("C-x g" . magit-status))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Monky magic 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package monky
  :ensure t
  :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Which key mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package which-key
  :diminish
  :custom
  (which-key-separator " ")
  (which-key-prefix-prefix "+")
  :config
  (which-key-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  neotree windows 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package neotree
  :bind ([f11] . neotree-toggle))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; discover my major 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package discover-my-major
  :bind ("C-h C-m" . discover-my-major))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Markdown mode support
;; requires multimarkdown application
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

(use-package groovy-mode
  :mode (("\\.groovy" . groovy-mode)
         ("/Jenkinsfile" . groovy-mode))
  ;; :pin manual
  :ensure t
  :interpreter ("groovy" . groovy-mode)
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ivy /swiper / counsel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
(use-package counsel
  :demand t
  :bind*
  (("C-c C-r" . ivy-resume)
   ;; ("M-a" . counsel-M-x)
   ("C-M-i" . counsel-imenu)
   ("C-x C-r" . counsel-recentf)
   ("C-x C-f" . counsel-find-file)
   ("C-c d" . counsel-dired-jump)
   ("C-c j" . counsel-git-grep)
   ("C-c C-a" . counsel-ag)
   ("C-c r" . counsel-rg)
   ("C-c l" . counsel-locate)
   ("M-y" . counsel-yank-pop))
  :bind (:map help-map
              ("f" . counsel-describe-function)
              ("v" . counsel-describe-variable)
              ("l" . counsel-info-lookup-symbol)
              :map swiper-map
              ("C-c M-s" . swiper-isearch-toggle)
              ;; ("M-%" . swiper-query-replace)
              :map isearch-mode-map
              ("C-c M-s" . swiper-isearch-toggle)
              )
  :config
  (ivy-mode 1)
  (setq counsel-find-file-at-point t)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-display-style 'fancy)
  (setq ivy-initial-inputs-alist nil)
  (setq ivy-dynamic-exhibit-delay-ms 200)
;; not sure about it  (setq ivy-extra-directories nil) ;; not . and .. in dired mode
  (setq counsel-rg-base-command
		"rg --sort path -M 120 --no-heading --line-number --color never %s")
  ;; better fuzzy matchings https://oremacs.com/2016/01/06/ivy-flx/
  (setq ivy-re-builders-alist
        '((ivy-switch-buffer . ivy--regex-plus)
	  (counsel-find-file . ivy--regex-plus)
          (t . ivy--regex-fuzzy)))
  (define-key ivy-minibuffer-map (kbd "<return>") 'ivy-alt-done)
  (define-key ivy-minibuffer-map (kbd "C-t") 'ivy-rotate-preferred-builders)
  ;; Ivy custom actions
  (define-key ivy-minibuffer-map (kbd "C-c o") 'ivy-occur)

  )

;; suggestion from :
;; https://emacs.stackexchange.com/questions/33701/do-not-open-dired-for-directories-when-using-counsel-find-file
;; (with-eval-after-load 'counsel (let ((done (where-is-internal
;; #'ivy-done ivy-minibuffer-map t)) (alt (where-is-internal
;; #'ivy-alt-done ivy-minibuffer-map t))) (define-key
;; counsel-find-file-map done #'ivy-alt-done) (define-key
;; counsel-find-file-map alt #'ivy-done)))

(use-package swiper
  :ensure t
  :bind*
  (("C-s" . swiper)
   ("C-r" . swiper)
   ("C-M-s" . swiper-all))
  :bind
  (:map read-expression-map
        ("C-r" . counsel-expression-history)))



;; tip from https://github.com/purcell/emacs.d/blob/master/lisp/init-ivy.el
;; droped support to ivy 
(define-key ivy-mode-map (kbd "M-s /") 'swiper-thing-at-point)

(use-package ivy-posframe
  :ensure t
  :diminish ""
  :custom
  (ivy-posframe-height-alist
   '((swiper . 15)
     (swiper-multi . 15)
     (swiper-all . 15)
     (t . 10)))
  (ivy-posframe-display-functions-alist
   '((complete-symbol . ivy-posframe-display-at-point)
     (swiper . nil)
     (swiper-multi . nil)
     (swiper-all . nil)
     (t . ivy-posframe-display-at-frame-center)))
  :config
  (ivy-posframe-mode 1))

(use-package ivy-prescient
  :after ivy
  :ensure t
  :config
  (ivy-prescient-mode)
  (prescient-persist-mode))

(use-package prescient)

(use-package ivy-prescient
  :after (ivy counsel)
  :config
  (ivy-prescient-mode 1))

(use-package ivy-rich
  :after (ivy counsel)
  :disabled ;; super CPU consuming 
  :custom
  (ivy-rich-parse-remote-buffer nil)
  (ivy-rich-parse-remote-file-path nil)
  (ivy-rich-path-style (quote full))
  :config
  (ivy-rich-mode 1))

(use-package ivy-hydra
  :after (ivy counsel)
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Counsel-projectile
;; It is advisable to create a .projectile file in the root dir of the project
;; See https://github.com/bbatsov/projectile
;;     https://github.com/ericdanan/counsel-projectile
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package counsel-projectile
  :ensure t
  :after (counsel projectile)
  :init  (counsel-projectile-mode)
  :commands (counsel-projectile
             counsel-projectile-switch-project
             counsel-projectile-find-file
             counsel-projectile-find-dir)
  :bind (([C-f1] . counsel-projectile-find-file))
  :config
  (progn
    (setq counsel-projectile-remove-current-buffer t)
    (setq counsel-projectile-remove-current-project t)
    )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hydra
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package hydra
  :defer 0.5
  :bind (("C-c p" . hydra-projectile/body)
         ("C-c h c" . hydra-clock/body)
;;         ("C-c f" . hydra-flycheck/body)
         ("C-c h f" . hydra-go-to-file/body)
         ("C-c h m" . hydra-magit/body)
         ("C-c h o" . hydra-org/body)
         ([S-f4] . hydra-windows/body)
         ("C-c h s" . smartparens-hydra/body))
  )

;; (use-package use-package-hydra
;;   :after hydra)

(require 'hydra)
(setq hydra--work-around-dedicated nil)
(hydra-add-font-lock)

(defhydra hydra-avy (:color teal)
  ("j" avy-goto-char)
  ("k" avy-goto-word-1)
  ("l" avy-goto-line)
  ("s" avy-goto-char-timer)
  ("f" counsel-find-file)
  ("q" nil))

(defhydra hydra-error (global-map "M-g")
  "goto-error"
  ("h" first-error "first")
  ("j" next-error "next")
  ("k" previous-error "prev")
  ("v" recenter-top-bottom "recenter")
  ("q" nil "quit"))

(global-set-key (kbd "M-g g") 'avy-goto-line)
(global-set-key (kbd "C-M-g") 'avy-goto-line)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)
(global-set-key (kbd "C-M-;") 'avy-goto-word-1)
(global-set-key (kbd "M-g s") 'avy-goto-subword-0)
(global-set-key (kbd "C-'") 'avy-goto-char-timer)


(defhydra hydra-clock (:color blue)
  "
  ^
  ^Clock^             ^Do^
  ^─────^─────────────^──^─────────
  _q_ quit            _c_ cancel
  ^^                  _d_ display
  ^^                  _e_ effort
  ^^                  _i_ in
  ^^                  _j_ jump
  ^^                  _o_ out
  ^^                  _r_ report
  ^^                  ^^
  "
  ("q" nil)
  ("c" org-clock-cancel)
  ("d" org-clock-display)
  ("e" org-clock-modify-effort-estimate)
  ("i" org-clock-in)
  ("j" org-clock-goto)
  ("o" org-clock-out)
  ("r" org-clock-report))

(defhydra hydra-go-to-file (:color blue)
  "
    ^
    ^Go To^           ^Config^            ^Agenda             ^Other^
    ^─────^───────────^──────^────────────^──────^────────────^─────^────────
    _q_ quit          ^^                  ^^                  ^^
    ^^                _ce_ emacs          _ct_ todo           _cj_ journal
    _h_ help          ^^                  ^^                  _cf_ fedora
    _o_ org-help      ^^                  ^^                  _cm_ mpls docs
    "
  ("q" nil)
  ("ce" (find-file "~/.emacs"))
  ("ct" (find-file my:org-agenda-todo))
  ("cj" (find-file "~/Documents/notes/work/daily.org"))
  ("cf" (find-file "~/Documents/notes/fedora.org"))
  ("cm" (dired "~/Documents/notes/work/mpls"))
  ("h" (find-file-read-only (expand-file-name "../Refcard/emacs.org" user-emacs-directory)))
  ("o" (find-file-read-only (expand-file-name "../Refcard/orgcard.org" user-emacs-directory)))
  )

(defhydra hydra-magit (:color blue)
  "
  ^
  ^Magit^             ^Do^
  ^─────^─────────────^──^────────
  _q_ quit            _b_ blame
  ^^                  _c_ clone
  ^^                  _i_ init
  ^^                  _s_ status
  ^^                  _l_ log-buffer-file
  ^^                  ^^
  "
  ("q" nil)
  ("b" magit-blame)
  ("c" magit-clone)
  ("i" magit-init)
  ("s" magit-status)
  ("l" magit-log-buffer-file)
  )

(defhydra hydra-org (:color blue)
  "
  ^
  ^Org^             ^Do^
  ^───^─────────────^──^─────────────
  _q_ quit          ^^
  ^^                _a_ agenda
  ^^                _c_ capture
  ^^                _d_ decrypt
  ^^                _i_ insert-link
  ^^                _k_ cut-subtree
  ^^                _o_ open-link
  ^^                _r_ refile
  ^^                _s_ store-link
  ^^                _t_ todo-tree
  ^^                ^^
  "
  ("q" nil)
  ("a" org-agenda)
  ("c" org-capture)
  ("d" org-decrypt-entry)
  ("k" org-cut-subtree)
  ("i" org-insert-link-global)
  ("o" org-open-at-point-global)
  ("r" org-refile)
  ("s" org-store-link)
  ("t" org-show-todo-tree))

(defhydra hydra-projectile (:color blue)
  "
  ^
  ^Projectile^        ^Buffers^           ^Find^              ^Search^
  ^──────────^────────^───────^───────────^────^──────────────^──────^────────────
  _q_ quit            _b_ list            _d_ directory       _r_ replace
  _i_ reset cache     _K_ kill all        _D_ root            _R_ regexp replace
  ^^                  _S_ save all        _f_ file            _s_ ag
  ^^                  ^^                  _p_ project         ^^
  ^^                  ^^                  ^^                  ^^
  "
  ("q" nil)
  ("b" counsel-projectile-switch-to-buffer)
  ("d" counsel-projectile-find-dir)
  ("D" projectile-dired)
  ("f" counsel-projectile-find-file)
  ("i" projectile-invalidate-cache :color red)
  ("K" projectile-kill-buffers)
  ("p" counsel-projectile-switch-project)
  ("r" projectile-replace)
  ("R" projectile-replace-regexp)
  ("s" counsel-projectile-ag)
  ("S" projectile-save-project-buffers))

(defhydra hydra-windows (:color pink)
  "
  ^
  ^Windows^           ^Window^            ^Zoom^
  ^───────^───────────^──────^────────────^────^──────
  _q_ quit            _b_ balance         _-_ out
  ^^                  _i_ heighten        _+_ in
  ^^                  _j_ narrow          _=_ reset
  ^^                  _k_ lower           ^^
  ^^                  _l_ widen           ^^
  ^^                  _s_ swap            ^^
  ^^                  ^^                  ^^
  "
  ("q" nil)
  ("b" balance-windows)
  ("i" enlarge-window)
  ("j" shrink-window-horizontally)
  ("k" shrink-window)
  ("l" enlarge-window-horizontally)
  ("s" window-swap-states :color blue)
  ("-" text-scale-decrease)
  ("+" text-scale-increase)
  ("=" (text-scale-increase 0)))

(defhydra smartparens-hydra (:hint nil)
  "
_d_: down           _a_: back-down        _f_: -> sexp    _k_: hyb-kill      _c_-_a_: begin
_e_: up             _u_: back-up          _b_: <- sexp    _K_: kill          _c_-_e_: end
_[_: back-unwrap    _]_: unwrap           _r_: rewrap     _m_: mark            _j_: join
_p_: prev           _n_: next             _c_: copy       _s_: mark-thing      _|_: split
_t_: transpose      _T_: hyb-transpose    _q_: quit
"

  ("d" sp-down-sexp)
  ("e" sp-up-sexp)
  ("u" sp-backward-up-sexp)
  ("a" sp-backward-down-sexp)
  ("f" sp-forward-sexp)
  ("b" sp-backward-sexp)
  ("k" sp-kill-hybrid-sexp)
  ("t" sp-transpose-sexp)
  ("T" sp-transpose-hybrid-sexp)
  ("K" sp-kill-sexp)
  ("[" sp-backward-unwrap-sexp)
  ("]" sp-unwrap-sexp)
  ("r" sp-rewrap-sexp)
  ("p" sp-previous-sexp)
  ("n" sp-next-sexp)
  ("j" sp-join-sexp)
  ("|" sp-split-sexp)
  ("c" sp-copy-sexp)
  ("s" sp-select-next-thing :color blue)
  ("m" sp-mark-sexp :color blue)
  ("SPC" nil "Cancel")
  ("q" nil :color blue))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; smerge mode - for Emacs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package smerge-mode
  :requires (hydra magit)
  :config
  (defhydra unpackaged/smerge-hydra
    (:color pink :hint nil :post (smerge-auto-leave))
    "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
    ("n" smerge-next)
    ("p" smerge-prev)
    ("b" smerge-keep-base)
    ("u" smerge-keep-upper)
    ("l" smerge-keep-lower)
    ("a" smerge-keep-all)
    ("RET" smerge-keep-current)
    ("\C-m" smerge-keep-current)
    ("<" smerge-diff-base-upper)
    ("=" smerge-diff-upper-lower)
    (">" smerge-diff-base-lower)
    ("R" smerge-refine)
    ("E" smerge-ediff)
    ("C" smerge-combine-with-next)
    ("r" smerge-resolve)
    ("k" smerge-kill-current)
    ("ZZ" (lambda ()
            (interactive)
            (save-buffer)
            (bury-buffer))
     "Save and bury buffer" :color blue)
    ("q" nil "cancel" :color blue))
  :hook (magit-diff-visit-file . (lambda ()
                                   (when smerge-mode
                                     (unpackaged/smerge-hydra/body)))))

;; helm-make
(use-package helm-make
  :disabled
  :commands (helm-make helm-make-projectile)
  :init (defalias 'hm 'helm-make)
  :config (setq helm-make-completion-method 'ivy))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Vim Powerline - for Emacs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package powerline
  :demand
  :disabled
  :defer
  :ensure t)

;;
;; Compile package
;;
(use-package compile
    :diminish compilation-in-progress
    :config
    (setq compilation-ask-about-save nil)
    ;; (setq compilation-scroll-output 'next-error)
	(setq compilation-skip-threshold 2)
    (setq compilation-always-kill t
          compilation-context-lines 10
          compilation-window-height 100
          compilation-scroll-output 'first-error)
    )

;; Quickly switch windows in Emacs 
;; Ace window
(use-package ace-window
  :bind ("C-x o" . ace-window)
  :commands ace-window)

(use-package transpose-frame
  :config (bind-key "C-x 4 t" #'transpose-frame))

;; window mover with the S-<arrows>
(use-package windmove
  :config
  ;; use shift + arrow keys to switch between visible buffers
  (windmove-default-keybindings))
;;
;; writegood-mode highlights bad word choices and has functions for calculating readability. 
;;
(use-package writegood-mode
  :ensure t
  :bind ("C-c g" . writegood-mode)
  :config
  (add-to-list 'writegood-weasel-words "actionable"))


;;
;; Fonts 
;;
;; requires: dnf -y install adobe-source-code-pro-fonts \
;; levien-inconsolata-fonts.noarch
;;
(defun set-frame-font-inconsolata (size &optional frames)
  "Set font to Inconsolata:pixelsize=SIZE:antialias=true:autohint=false.
Argument FRAMES has the same meaning as for `set-frame-font'"
  (interactive "n[Inconsolata] size: ")
  (set-frame-font (format "Inconsolata:pixelsize=%d:antialias=true:autohint=true" size) nil frames))

(defun set-frame-font-code-pro (size &optional frames)
  "Set font to Source Code Pro:pixelsize=SIZE:antialias=true:autohint=false.
Argument FRAMES has the same meaning as for `set-frame-font'"
  (interactive "n[Source Code Pro] size: ")
  (set-frame-font (format "Source Code Pro:pixelsize=%d:antialias=true:autohint=true" size) nil frames))

(add-to-list 'default-frame-alist '(font . "Inconsolata-13"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Python
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python3" . python-mode)
  :bind(:map
        python-mode-map
        ("C-c r" . onc/run-current-file)))

(use-package elpy
  :after python-mode
  :commands elpy-enable
  :custom
  (elpy-rpc-python-command "python3")
  (elpy-rpc-backend "jedi")
  (elpy-use-cpython "/usr/bin/python3")
  :config
  (elpy-enable)

  (delete 'elpy-module-company elpy-modules)

  (add-hook 'python-mode-hook
            (lambda ()
              (company-mode)
              (add-to-list 'company-backends
                           (company-mode/backend-with-yas 'elpy-company-backend)))))
(put 'scroll-left 'disabled nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mail settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This, somehow causes a huge problem in loading the notmuch-message mode causing
;; failures likes autoloading failed loading function org
;;
;; (add-hook 'message-mode-hook 'visual-line-mode)
;; (add-hook 'message-mode-hook 'flyspell-mode)
(use-package message
  :if emacs-work-enabled
  :config (progn
;;            (add-hook 'message-mode-hook 'flyspell-mode)

            (setq message-citation-line-format "On %a, %b %e, %Y at %R %p, %f wrote:\n"
                  message-citation-line-function 'message-insert-formatted-citation-line)))
;; setup the mail address and use name
(setq mail-user-agent 'message-user-agent)
(setq user-mail-address "manap@intracom-telecom.com"
      user-full-name "Apostolos Manolitzas")
;; smtp config
(setq smtpmail-smtp-server "iris.intracomtel.com"
      message-send-mail-function 'message-smtpmail-send-it)

;; report problems with the smtp server
(setq smtpmail-debug-info t)
;; add Cc and Bcc headers to the message buffer
(setq message-default-mail-headers "Cc: \nBcc: \n")
;; postponed message is put in the following draft directory
(setq message-auto-save-directory "~/mail/draft")
(setq message-kill-buffer-on-exit t)
;; change the directory to store the sent mail
(setq message-directory "~/mail/")
;; from https://github.com/legoscia/messages-are-flowing
;; (add-hook 'message-mode-hook 'messages-are-flowing-use-and-mark-hard-newlines)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Notmuch
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package notmuch
  :if emacs-work-enabled
  :ensure t
  :demand t
  :defer t
  :bind (:map notmuch-search-mode-map
              ("g" . notmuch-refresh-this-buffer)
              :map notmuch-tree-mode-map
              ("g" . notmuch-refresh-this-buffer))
  :config
  ;; (require 'config-notmuch)

  ;; (setq notmuch-search-oldest-first nil)
  ;; (setq notmuch-fcc-dirs
  ;;       '(("ttuegel@mailbox.org" . "mailbox/INBOX +sent +mailbox")
  ;;         ("ttuegel@gmail.com" . "gmail/all_mail +sent +gmail")
  ;;         ("tuegel2@illinois.edu" . "illinois/INBOX +sent +illinois")))
  (setq notmuch-saved-searches
        '(
		  (:name "inbox" :query "tag:inbox" :key "i")
		  (:name "unread" :query "tag:unread" :key "e")
		  (:name "unread inbox" :query "tag:unread and tag:inbox" :key "u")
		  (:name "flagged" :query "tag:flagged" :key "f")
		  (:name "sent" :query "tag:sent" :key "s")
		  (:name "drafts" :query "tag:draft" :key "d")
		  (:name "all mail" :query "*" :key "a")
		  (:name "week inbox" :query "date:\"this week\" and not tag:list" :key "o")
		  (:name "today" :query "date:today" :key "t")
		  (:name "today,no-list" :query "date:today and not tag:list" :key "y")
		  (:name "last week" :query "date:\"last week\" and not tag:list" :key "w")
          ))
  (progn
  (define-key notmuch-search-mode-map "d"
    (lambda ()
      "toggle deleted tag for thread"
      (interactive)
      (if (member "deleted" (notmuch-search-get-tags))
          (notmuch-search-tag '("-deleted"))
        (notmuch-search-tag '("+deleted" "-inbox" "-unread")))))

  (define-key notmuch-search-mode-map "!"
    (lambda ()
      "toggle unread tag for thread"
      (interactive)
      (if (member "unread" (notmuch-search-get-tags))
          (notmuch-search-tag '("-unread"))
        (notmuch-search-tag '("+unread")))))


  (define-key notmuch-show-mode-map "d"
    (lambda ()
      "toggle deleted tag for message"
      (interactive)
      (if (member "deleted" (notmuch-show-get-tags))
          (notmuch-show-tag '("-deleted"))
        (notmuch-show-tag '("+deleted" "-inbox" "-unread")))))

  (define-key notmuch-search-mode-map "a"
    (lambda ()
      "toggle archive"
      (interactive)
      (if (member "archive" (notmuch-search-get-tags))
          (notmuch-search-tag '("-archive"))
        (notmuch-search-tag '("+archive" "-inbox" "-unread")))))


  (define-key notmuch-show-mode-map "a"
    (lambda ()
      "toggle archive"
      (interactive)
      (if (member "archive" (notmuch-show-get-tags))
          (notmuch-show-tag '("-archive"))
        (notmuch-show-tag '("+archive" "-inbox" "-unread")))))


  (define-key notmuch-hello-mode-map "i"
    (lambda ()
      (interactive)
      (notmuch-hello-search "tag:inbox")))

  (define-key notmuch-hello-mode-map "u"
    (lambda ()
      (interactive)
      (notmuch-hello-search "tag:unread")))

  (define-key notmuch-hello-mode-map "a"
    (lambda ()
      (interactive)
      (notmuch-hello-search "tag:archive")))

  ;; https://www.reddit.com/r/emacs/comments/9jp9zt/anyone_know_what_variable_binding_depth_exceeds/
  (setq max-specpdl-size 33000)
  )
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; org-mode settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package org
  :ensure org-contrib
  ;; :pin org
  :commands (org
             org-capture
             org-mode
             org-store-link
             )
  :mode (("\\.org$" . org-mode)
         ("\\.org_archive\\$" . org-mode))
  :bind (("C-c a" . org-agenda)
         ("C-c F" . counsel-org-file)
         ([f6] . org-capture)
         ([S-f6] . org-store-link)
         )
  ;; :hook flyspell-mode
  :config (progn
            (setq org-agenda-files my:org-agenda-files)
            (setq org-agenda-include-diary t)
            (setq org-refile-use-outline-path 'file)
            (setq org-refile-targets my:org-refile-targets)
            (setq org-outline-path-complete-in-steps nil)
            ;; From: http://www.totherme.org/configs/org-stuff.html#orgheadline11
            (setq org-todo-keywords '((sequence "TODO(t)" "NEXT(x)"  "|" "DONE(d!)" "NOTDOING(n@/!)" "LATER(l)" "WAITING(w)")
                                      (sequence "EMAIL(e)" "REPLY(r)")))

            (setq org-todo-keyword-faces
                  (quote (("TODO" :foreground "red" :weight bold)
                          ("NEXT" :foreground "blue" :weight bold)
                          ("DONE" :foreground "green" :weight bold)
                          ("WAITING" :foreground "orange" :weight bold)
                          ("NOTDOING" :foreground "green" :weight bold)
                          ("LATER" :foreground "orange" :weight bold)
                          ("EMAIL" :foreground "red" :weight bold)
                          ("REPLY" :foreground "red" :weight bold))))


            (setq org-capture-templates
                  '(("t" "TODO" entry (file+headline my:org-agenda-todo "Collect")
                     "* TODO %? %^G \n  %U" :empty-lines 1)
                    ("s" "Scheduled TODO" entry (file+headline my:org-agenda-todo "Collect")
                     "* TODO %? %^G \nSCHEDULED: %^t\n  %U" :empty-lines 1)
                    ("d" "Deadline" entry (file+headline my:org-agenda-todo "Collect")
                     "* TODO %? %^G \n  DEADLINE: %^t" :empty-lines 1)
                    ("p" "Priority" entry (file+headline my:org-agenda-todo "Collect")
                     "* TODO [#A] %? %^G \n  SCHEDULED: %^t")
                    ("a" "Appointment" entry (file+headline my:org-agenda-todo "Collect")
                     "* %? %^G \n  %^t")
                    ("l" "Link" entry (file+headline my:org-agenda-todo "Collect")
                     "* TODO %a %? %^G\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n")
                    ("n" "Note" entry (file+headline my:org-agenda-todo "Notes")
                     "* %? %^G\n%U" :empty-lines 1)
                    ("j" "Journal" entry (file+datetree "~/Documents/notes/work/daily.org")
                     "* %? %^G\nEntered on %U\n")))
            ;; src block indentation / editing / syntax highlighting
            (setq org-src-fontify-natively t
                  org-src-window-setup 'current-window ;; edit in current window
                  org-src-strip-leading-and-trailing-blank-lines t
                  org-src-preserve-indentation t ;; do not put two spaces on the left
                  org-src-tab-acts-natively t)
            (use-package org-protocol
              :after org
              :config
              (add-to-list 'org-capture-templates
                           `("c" "org-protocol-capture" entry (file my:org-agenda-todo)
                             "* TODO [[%:link][%:description]]\n\n %i"
                             :immediate-finish t)
                           )
              )
            )
  (setq org-log-done t)
  )

;; Avoid `org-babel-do-load-languages' since it does an eager require.
(use-package ob-python
  :defer t
  :ensure org-contrib
  :commands (org-babel-execute:python))

(use-package ob-shell
  :defer t
  :ensure org-contrib
  :commands
  (org-babel-execute:sh
   org-babel-expand-body:sh

   org-babel-execute:bash
   org-babel-expand-body:bash))

(use-package ob-plantuml
  :defer t
  :ensure org-contrib
  :commands (org-babel-execute:plantuml
             org-babel-prep-session:plantuml
             org-babel-variable-assignments:plantuml))

(use-package ob-C
  :commands (org-babel-execute:C
             org-babel-expand-body:C))

(use-package ob-C++
  :commands (org-babel-execute:C++
             org-babel-expand-body:C++))

(use-package ob-dot
  :commands (org-babel-execute:dot
             org-babel-expand-body:dot))

(use-package ob-emacs-lisp
  :commands (org-babel-execute:emacs-lisp
             org-babel-expand-body:emacs-lisp))

(use-package ol-notmuch
  :load-path "lisp/"
  :after (org notmuch))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode))

(use-package org-journal
  :ensure t
  :bind (("C-c t" . org-journal-new-entry)
         ;;         ("C-c C-y" . journal-file-yesterday)
         )
  :custom
  (org-journal-dir "~/Documents/notes/work/journal/")
  (org-journal-file-format "%Y%m%d")
  (org-journal-date-format "%e %b %Y (%A)")
  (org-journal-time-format "")
  (org-journal-file-type 'monthly)
  :preface
  (defun get-journal-file-yesterday ()
    "Gets filename for yesterday's journal entry."
    (let* ((yesterday (time-subtract (current-time) (days-to-time 1)))
           (daily-name (format-time-string "%Y%m%d" yesterday)))
      (expand-file-name (concat org-journal-dir daily-name))))

  (defun journal-file-yesterday ()
    "Creates and load a file based on yesterday's date."
    (interactive)
    (find-file (get-journal-file-yesterday)))
  :config
  (progn
    (add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode)))
    )

;; "org-protocol:/sub-protocol:/" triggers actions associated with sub-protocol through the custom variable org-protocol-protocol-alist.
;; Linux setup (Gnome)

;; For this to work, you'll need the Gnome-Libraries to be installed.

;; gconftool-2 -s /desktop/gnome/url-handlers/org-protocol/command '/usr/local/bin/emacsclient %s' --type String
;; gconftool-2 -s /desktop/gnome/url-handlers/org-protocol/enabled --type Boolean true

;; Description=A protocol for org-mode


;; (setq org-capture-templates
  ;;   (quote (("w" "Weblink" entry (file (expand-file-name "bookmarks.org" "~/org/"))
  ;;            "* %c\n  :PROPERTIES:\n  :CREATED: %U\n  :END:\n  - Quote:\n    %i" :unnarrowed))))
  ;; )

;; https://github.com/abo-abo/org-download
(use-package org-download
  :demand t
  :hook ((dired-mode . org-download-enable)))
;; (profiler-report)
;; (profiler-stop)

(use-package calendar
  :config
  (setq calendar-mark-diary-entries-flag t)
  (setq calendar-time-display-form
        '(24-hours ":" minutes
                   (when time-zone
                     (concat " (" time-zone ")"))))
  (setq calendar-week-start-day 1)      ; Monday
  (setq calendar-date-style 'iso)
  (setq calendar-christian-all-holidays-flag t)
  (setq calendar-holidays
        (append holiday-local-holidays  ; TODO set local holidays
                holiday-solar-holidays))

  (use-package solar
    :config
    (setq calendar-latitude 35.17
          calendar-longitude 33.36))

  (use-package lunar
    :config
    (setq lunar-phase-names
          '("New Moon"
            "First Quarter Moon"
            "Full Moon"
            "Last Quarter Moon")))

  :hook (calendar-today-visible-hook . calendar-mark-today))

(when (window-system)
  (set-frame-height (selected-frame) 50)
  (set-frame-width (selected-frame) 120) 
  (set-frame-position (selected-frame) 350 30))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; groovy mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package smartscan
  :defer t
  :ensure t
  )

(use-package groovy-mode
  :defer t
  :ensure t
  :pin melpa
  :config
  (setq-default groovy-indent-offset 2)
  ;; use groovy-mode when file ends in .groovy or has #!/bin/groovy at start
  (add-hook 'groovy-mode-hook 'smartscan-mode)
  :mode (("\\.groovy" . groovy-mode)
         ("\\.gradle$" . groovy-mode)
         ("/Jenkinsfile" . groovy-mode))
  :interpreter ("groovy" . groovy-mode)
  )

(use-package font-core
  :config
  ;; turn on syntax highlighting
  (global-font-lock-mode 1))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dockerfile  mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package dockerfile-mode
  :ensure t
  :mode "Dockerfile\\'")

(use-package docker-compose-mode
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; undo-tree  (not sure) mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package undo-tree
  ;;  :bind (("M-/" . undo-tree-visualize))
  :disabled
  :diminish
  :config
  (global-undo-tree-mode)
  (setq undo-tree-visualizer-diff t)
  (setq undo-tree-visualizer-timestamps t)
  )

;; justfile
(use-package makefile-mode
  :mode (("justfile" . makefile-mode)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; string-inflection
;; used for switching between different cases, eg CamelCase,
;; lowerCamelCase, snake_case, and SCREAMING_SNAKE_CASE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (use-package string-inflection
;;   :defer t
;;   ;; :bind (("C-c c i" . string-inflection-cycle)
;;   ;;        ("C-c c l" . string-inflection-lower-camelcase)
;;   ;;        ("C-c c c" . string-inflection-camelcase)
;;   ;;        ("C-c c s" . string-inflection-underscore)
;;   ;;        ("C-c c u" . string-inflection-upcase))
;;   :hydra (hydra-string-inflection
;;           ()
;;           "Inflection"
;;           ("i" string-inflection-cycle "Cycle")
;;           ("l" string-inflection-lower-camelcase "camelCase")
;;           ("c" string-inflection-camelcase "CamelCase")
;;           ("s" string-inflection-underscore "Underscore")
;;           ("u" string-inflection-upcase "Upcase")
;;           ("q" nil "Quit" :color blue)
;;           ("g" nil "Quit" :color blue))
;; )

;; vterm
(use-package vterm
  :ensure
  :commands vterm
  :config
  (setq vterm-disable-bold-font nil)
  (setq vterm-disable-inverse-video nil)
  (setq vterm-disable-underline nil)
  (setq vterm-kill-buffer-on-exit nil)
  (setq vterm-max-scrollback 9999)
  (setq vterm-shell "/bin/bash")
  (setq vterm-term-environment-variable "xterm-256color")
  (add-hook 'vterm-mode (lambda () (setq-local global-hl-line-mode nil)))
  (add-hook 'vterm-copy-mode (lambda () (call-interactively 'hl-line-mode)))
  )

(use-package symbol-overlay
  :diminish symbol-overlay-mode
  :hook ((prog-mode html-mode yaml-mode conf-mode) . symbol-overlay-mode)
  :bind (("M-i" . symbol-overlay-put)
         ("M-n" . symbol-overlay-jump-next)
         ("M-p" . symbol-overlay-jump-prev)
         ("M-N" . symbol-overlay-switch-forward)
         ("M-P" . symbol-overlay-switch-backward)
         ("M-C" . symbol-overlay-remove-all))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tramp setup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package tramp
  :defer t
  ;; :load-path "/tmp/mytramp/usr/local/share/emacs/site-lisp/"
  :config
  ;; (setq tramp-verbose 6)

  ;; upstream value, takes into account recent ssh messages
  (setq tramp-yesno-prompt-regexp (concat
                                   (regexp-opt
                                    '("Are you sure you want to continue connecting (yes/no)?"
                                      "Are you sure you want to continue connecting (yes/no/[fingerprint])?")
                                    t)
                                   "\\s-*"))

  ;; disable vc for remote files (speed increase)
  (setq vc-ignore-dir-regexp
        (format "\\(%s\\)\\|\\(%s\\)"
                vc-ignore-dir-regexp
                tramp-file-name-regexp))
  ;; :pin manual
  )

(use-package friendly-tramp-path
  :disabled
  :after tramp)

;; add more paths to search path 
(use-package tramp-sh
  :after tramp
  :config (progn
			(add-to-list 'tramp-remote-path "~/.cargo/bin")
			(add-to-list 'tramp-remote-path "/opt/clang35/bin")
			(add-to-list 'tramp-remote-path "~/bin"))
  )

(use-package protobuf-mode
  :ensure t
  :mode (("\\.proto\\`" . protobuf-mode)
         ("\\.fbs\\`" . protobuf-mode))
  )


;; man support 
(use-package info-colors
  :after info-mode
  :hook
  (Info-selection #'info-colors-fontify-node))


(use-package man
  :defer t
  :custom
  (Man-notify-method 'pushy "show manpage HERE")
  :custom-face
  (Man-overstrike ((t (:inherit font-lock-type-face :bold t))))
  (Man-underline ((t (:inherit font-lock-keyword-face :underline t)))))

(use-package woman
  :defer t
  :custom-face
  (woman-bold ((t (:inherit font-lock-type-face :bold t))))
  (woman-italic ((t (:inherit font-lock-keyword-face :underline t)))))

;; https://github.com/dlip/nixconfig/blob/42c3da13e8213d04ef6455dcd61daea86a0edb15/home/emacs/config/init/disabled_regex.el
;; https://github.com/jwiegley/regex-tool
;; https://www.reddit.com/r/emacs/comments/n0dtm7/bridging_islands_in_emacs_rebuilder_and/
;; https://karthinks.com/software/bridging-islands-in-emacs-1/#
;; https://www.reddit.com/r/emacs/comments/mil5to/use_visualreplaceregexp_lisp_functions_to_do/
;; https://francismurillo.github.io/2017-03-30-Exploring-Emacs-rx-Macro/

;; (use-package re-builder
;;   :commands (re-builder
;;              regexp-builder)
;;   :validate-custom
;;   (reb-re-syntax 'string))


;; https://github.com/jcs-elpa/docstr
;; (use-package docstr
;;   :after cc-mode
;;   :hook
;;   (cc-mode . docstr-mode)
;;   (c-mode . docstr-mode)
;;   :config
;;   (setq docstr-key-support t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dogears
;; https://github.com/alphapapa/dogears.el/tree/master
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package dogears
  :after helm
  ;; These bindings are optional, of course:
  :bind (:map global-map
              ("M-g d" . dogears-go)
              ("M-g M-r" . dogears-remember)
              ("M-g M-b" . dogears-back)
              ("M-g M-f" . dogears-forward)
              ("M-g M-d" . dogears-list)
              ("M-g M-D" . dogears-sidebar))
  )


