# .emacs file and partners

# Packages used in emacs
* browse-kill-ring    interactively insert items from kill-ring
* clang-format        Format code using clang-format
* cmake-mode          major-mode for editing CMake sources
* cmake-project       Integrates CMake build process with Emacs
* column-enforce-...  Highlight text that extends beyond a  column
* company-ycmd        company-mode backend for ycmd
* dired+              Extensions to Dired.
* etags-select        Select from multiple tags
* expand-region       Increase selected region by semantic units.
* flycheck-plantuml   Integrate plantuml with flycheck
* flycheck-rust       Flycheck: Rust additions and Cargo support
* flycheck-yang       YANG flycheck checker
* flycheck-ycmd       flycheck integration for ycmd
* flymake             a universal on-the-fly syntax checker
* flymake-cursor      displays flymake error msg in minibuffer after delay
* goto-last-change    Move point through buffer-undo-list positions
* helm-flycheck       Show flycheck errors with helm
* helm-projectile     Helm integration for Projectile
* highlight-curre...  highlight line where the cursor is.
* highlight-symbol    automatic and manual symbol highlighting
* magit               A Git porcelain inside Emacs.
* mark-more-like-...  Mark additional regions in buffer matching current region.
* mark-multiple       Sorta lets you mark several regions at once.
* markdown-mode       Major mode for Markdown-formatted text
* neotree             A tree plugin like NerdTree for Vim
* pydoc               functional, syntax highlighted pydoc navigation
* racer               code completion, goto-definition and docs browsing for Rust via racer
* rainbow-delimiters  Highlight brackets according to their depth
* rg                  A search tool based on ripgrep.
* use-package         A configuration macro for simplifying your .emacs
* volatile-highli...  Minor mode for visual feedback on some operations.
* yasnippet           Yet another snippet extension for Emacs.
* yasnippet-yasnippet Yet another snippet extension for Emacs.
* zeal-at-point       Search the word at point with Zeal
* org-mode, org-ref, org-capture
* pdftools



## How to install the needed packages

You can get a list of currently installed packages (excluding built in
packages) from the variable package-activated-list. To automatically
install them on startup, see this question: how to automatically
install emacs packages by specifying a list of package names?

More specifically, if you do C-h v package-activated-list, copy the
value shown, and insert it as the value of prelude-packages, emacs
will automatically ensure those packages are installed on start up.

## Interesting emacs setup around the web

1. [Setup with ivy,swiper, counsel](https://github.com/jethrokuan/emacs-workshop/blob/master/thoughtspeed-motion.org)
2. [ivy setup](https://github.com/abo-abo/oremacs/blob/github/modes/ora-ivy.el)
3. [ivy, swiper, counsel
   presentation](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html)
   

## Cask and usage

